package pl.edu.uwm.wmii.jasinskimichal.Test2;

import pl.edu.uwm.wmii.jasinskimichal.Test2.pl.imiajd.Jasinski.Komputer;
import pl.edu.uwm.wmii.jasinskimichal.Test2.pl.imiajd.Jasinski.Laptop;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<>();
        grupa.add(new Komputer("Asus", 2000, 2, 3));
        grupa.add(new Komputer("Sony", 2001, 2, 3));
        grupa.add(new Komputer("Adidas", "2000-02-02"));
        grupa.add(new Komputer("Nike", 2000, 2, 3));
        grupa.add(new Komputer("Adidas", "2013-03-02"));
        for (Komputer k : grupa) {
            System.out.println(k);
        }
        grupa.sort(null);
        System.out.println("\nPo sortowaniu: \n");
        for (Komputer k : grupa) {
            System.out.println(k);
        }
        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();
        grupaLaptopow.add(new Laptop("Sony", "2000-01-03", true));
        grupaLaptopow.add(new Laptop("Huawei", "2005-03-02", true));
        grupaLaptopow.add(new Laptop("Sony", "1993-01-02", false));
        grupaLaptopow.add(new Laptop("Sony", "1993-01-02", true));
        grupaLaptopow.add(new Laptop("Apple", "2005-03-02", false));
        System.out.println("--------------------------------------------------------------------------------------------");
        for (Laptop l : grupaLaptopow) {
            System.out.println(l);
        }
        grupaLaptopow.sort(null);
        System.out.println("\nPo sortowaniu: \n");
        for (Laptop l : grupaLaptopow) {
            System.out.println(l);
        }
    }
}
