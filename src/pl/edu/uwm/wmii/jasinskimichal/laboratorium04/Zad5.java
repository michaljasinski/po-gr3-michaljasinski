package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

import java.math.BigDecimal;

public class Zad5 {
    public static void main(String[]args)
    {


        System.out.println(foo(5, 250500, 4));
    }

        public static BigDecimal foo(int n, int k, int p) {
        BigDecimal wynik = BigDecimal.valueOf(k);

        for(int i = 0; i < n; i++) {
            wynik = wynik.add(wynik.divide(BigDecimal.valueOf(100)).multiply(BigDecimal.valueOf(p)));
        }

        return wynik;
    }

}
