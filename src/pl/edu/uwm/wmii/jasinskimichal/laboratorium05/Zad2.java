package pl.edu.uwm.wmii.jasinskimichal.laboratorium05;

import java.util.ArrayList;

public class Zad2 {
    public static void main(String[]args)
    {
        ArrayList<Integer> a = new ArrayList();
        ArrayList<Integer> b = new ArrayList();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(7);

        b.add(2);
        b.add(4);
        b.add(4);
        b.add(6);
        b.add(8);
        b.add(8);
        b.add(8);

        ArrayList test = merge(a,b);
        System.out.println(test);
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();
        int i=0;
        int j=0;
        while(wynik.size()<a.size()+b.size())
        {
            if(i<a.size())
            {
                wynik.add(a.get(i));
                i++;
            }
            if(j<b.size())
            {
                wynik.add(b.get(j));
                j++;
            }
        }
        return wynik;
    }
}
