package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

public class Zad1g {
    public static void main(String []args)
    {
        System.out.println(nice("1000000"));
    }
    public static String nice(String str)
    {
        StringBuffer tmp = new StringBuffer();
        int dotrzech=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            tmp.append(str.charAt(i));
            dotrzech++;
            if(dotrzech==3)
            {
                tmp.append("'");
                dotrzech=0;
            }
        }
        String wynik = tmp.reverse().toString();
        return wynik;
    }
}
