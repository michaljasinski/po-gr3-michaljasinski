package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium06;

public class RachunekBankowy {


    public RachunekBankowy(double saldo) {
        this.saldo = saldo;
    }
    public void obliczMiesieczneOdsetki()
    {
        double tmp =(saldo*rocznaStopaProcentowa) / 12;
        saldo+=tmp;
    }

    public double getSaldo() {
        return saldo;
    }

    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa/100;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RachunekBankowy that = (RachunekBankowy) o;
        return Double.compare(that.saldo, saldo) == 0;
    }

    @Override
    public String toString() {
        return "RachunekBankowy{" +
                "saldo=" + saldo +
                '}';
    }

    static double rocznaStopaProcentowa=0;
    private double saldo;
}
