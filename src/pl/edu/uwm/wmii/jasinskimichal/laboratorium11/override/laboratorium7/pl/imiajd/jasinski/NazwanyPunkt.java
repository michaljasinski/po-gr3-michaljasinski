package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski;

public class NazwanyPunkt extends Punkt {
    public NazwanyPunkt(int x, int y, String name)
    {
        super(x, y);
        this.name = name;
    }

    public void show()
    {
        System.out.println(name + ":<" + x() + ", " + y() + ">");
    }

    public NazwanyPunkt(int x, int y) {
        super(x, y);
    }

    @Override
    public int x() {
        return super.x();
    }

    @Override
    public int y() {
        return super.y();
    }

    @Override
    public String toString() {
        return "NazwanyPunkt{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private String name;
}

