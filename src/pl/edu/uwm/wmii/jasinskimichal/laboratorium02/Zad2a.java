package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2a {
    public static void main(String[] args)
    {

    Scanner scanner = new Scanner(System.in);
    System.out.println("Podaj n");
    int n = scanner.nextInt();
    int[]tab=new int[n];
    generuj(tab,n,-999,999);
    for(int x:tab)
    {
        System.out.println(x);
    }
        System.out.println(ileNieparzystych(tab));
        System.out.println(ileParzystych(tab));

    }
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc)
    {   Random rnd=new Random();
        int tmp;

        for(int i=0;i<n;i++)
        {
            tmp=rnd.nextInt(Math.abs(minWartosc)+maxWartosc+1)+minWartosc;
            tab[i]=tmp;


        }
    }
    public static int ileNieparzystych(int tab[])
    {
        int wynik=0;
        for(int x:tab)
        {
            if(x%2!=0)
            {
                wynik++;
            }

        }
        return wynik;
    }
    public static int ileParzystych(int tab[])
    {
        int wynik=0;
        for(int x:tab)
        {
            if(x%2==0)
            {
                wynik++;
            }

        }
        return wynik;
    }

}
