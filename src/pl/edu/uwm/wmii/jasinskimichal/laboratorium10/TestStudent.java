package pl.edu.uwm.wmii.jasinskimichal.laboratorium10;

import pl.edu.uwm.wmii.jasinskimichal.laboratorium10.pl.imiajd.jasinski.Student;

import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String [] args) throws CloneNotSupportedException {
        Student ja = new Student("Jasiński","2000-02-05",4.5);
        Student kowalski = new Student("Kowalski","2000-02-05",4.5);
        Student ja2 = new Student("Jasiński","2000-02-05",4.6);
        Student kowalski2 = new Student("Kowalski","1996-02-05",4.5);
        Student Maciek = new Student("Maciek","1990-03-05",2.2);
        Student Maciek2 = new Student("Maciek","1990-03-05",2.2);
        ArrayList<Student>tab = new ArrayList<>();
        tab.add(ja);tab.add(ja2);tab.add(kowalski);tab.add(kowalski2);tab.add(Maciek);
        System.out.println(kowalski.equals(kowalski2));
        System.out.println(Maciek.equals(Maciek2));
        System.out.println(tab);
        Collections.sort(tab);
        System.out.println(tab);

        Student Maciekclone = Maciek.clone();
        System.out.println(Maciekclone);
        Maciekclone.setSredniaOcen(2.3);
        System.out.println(Maciek);
        System.out.println(Maciekclone);


    }
}
