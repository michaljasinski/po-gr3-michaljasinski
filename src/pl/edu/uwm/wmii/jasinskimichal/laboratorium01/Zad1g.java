package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class Zad1g {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynikmno=1;
        int wynikdod=0;
        int an;
        String komunikatdod="";
        String komunikatmno="";
        for(int i = 0;i < n;i++)
        {   System.out.println("Podaj "+(i+1)+" liczbe");
            an = scanner.nextInt();
            wynikdod+= an;
            wynikmno*= an;
            if(an<0)
            {
                komunikatdod+="("+(Integer.toString(an) +")" + " + ");
                komunikatmno+="("+(Integer.toString(an) +")" + " * ");
            }
            else {
                komunikatdod += (Integer.toString(an) + " + ");
                komunikatmno += (Integer.toString(an) + " * ");
            }
        }
        komunikatdod=komunikatdod.substring(0,komunikatdod.length()-2);
        komunikatmno=komunikatmno.substring(0,komunikatmno.length()-2);
        komunikatdod+=("= "+Integer.toString(wynikdod));
        komunikatmno+=("= "+Integer.toString(wynikmno));
        System.out.println(komunikatdod + " oraz " + komunikatmno);
    }
}
