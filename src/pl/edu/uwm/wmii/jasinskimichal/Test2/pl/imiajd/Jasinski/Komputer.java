package pl.edu.uwm.wmii.jasinskimichal.Test2.pl.imiajd.Jasinski;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable<Komputer> {

    public Komputer(String nazwa, LocalDate dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }

    public Komputer(String nazwa, String dataProdukcji) {
        this.nazwa = nazwa;
        this.dataProdukcji = LocalDate.parse(dataProdukcji);
    }

    public Komputer(String nazwa, int rok, int miesiac, int dzien) {
        this.nazwa = nazwa;
        this.dataProdukcji = LocalDate.of(rok, miesiac, dzien);
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    public void setDataProdukcji(LocalDate dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    @Override
    public int compareTo(Komputer k) {
        if (nazwa.compareTo(k.nazwa) == 0) {
            return dataProdukcji.compareTo(k.dataProdukcji);
        }
        return nazwa.compareTo(k.nazwa);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null || getClass() != otherObject.getClass()) return false;
        Komputer other = (Komputer) otherObject;
        return nazwa.equals(other.nazwa) && dataProdukcji.equals(other.dataProdukcji);
    }

    @Override
    public Komputer clone() throws CloneNotSupportedException {
        Komputer cloned = (Komputer) super.clone();
        return cloned;
    }

    @Override
    public String toString() {
        return "Komputer: " + "nazwa: " + nazwa + ", data produkcji: " + dataProdukcji;
    }

    private String nazwa;
    private LocalDate dataProdukcji;

}
