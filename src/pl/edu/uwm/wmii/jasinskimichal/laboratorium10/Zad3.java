package pl.edu.uwm.wmii.jasinskimichal.laboratorium10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[]args) throws FileNotFoundException {
        ArrayList<String> tab = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String nazwa = scanner.next();
        Scanner plik = new Scanner(new File(nazwa));
        while(plik.hasNextLine())
        {
            tab.add(plik.nextLine());
        }
        System.out.println(tab);
        Collections.sort(tab);
        System.out.println(tab);

    }
}
