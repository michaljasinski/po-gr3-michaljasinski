package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class Zad1d {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynik=0;
        int an;
        String komunikat="";
        for(int i = 0;i < n;i++)
        {   System.out.println("Podaj "+(i+1)+" liczbe");
            an = scanner.nextInt();
            wynik+= Math.sqrt(Math.abs(an));
            if(an<0)
            {
                komunikat+="pierwiastek z |("+(Integer.toString(an) +")|" + " + ");
            }
            else {
                komunikat += ("pierwiastek z |"+Integer.toString(an)+"|" + " + ");
            }
        }
        komunikat=komunikat.substring(0,komunikat.length()-2);
        komunikat+=("= "+Integer.toString(wynik));
        System.out.println(komunikat);
    }
}
