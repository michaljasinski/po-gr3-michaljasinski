package pl.edu.uwm.wmii.jasinskimichal.Test2;

import java.util.LinkedList;
import java.util.ListIterator;

public class Zad2 {
    public static void main(String[] args) {
        LinkedList<String> tab = new LinkedList<>();
        tab.add("1.Asus");
        tab.add("2.Sony");
        tab.add("3.Huawei");
        tab.add("4.Acer");
        tab.add("5.Asus");
        tab.add("6.Playstation");
        tab.add("7.Xbox");
        for (String s : tab) {
            System.out.println(s);
        }
        redukuj(tab, 2);
        System.out.println("\nPo uzyciu redukuj: \n");
        for (String s : tab) {
            System.out.println(s);
        }
        redukuj(tab, 1);
        System.out.println("\nPo uzyciu redukuj: \n");
        for (String s : tab) {
            System.out.println(s);
        }
    }

    public static void redukuj(LinkedList<String> komputery, int n) {
        if (n < 1)
            return;
        int licz = 0;
        ListIterator iterator = komputery.listIterator();

        while (iterator.hasNext()) {
            if (licz == n - 1) {
                iterator.next();
                iterator.remove();
                licz = 0;
            } else {
                iterator.next();
                licz++;
            }


        }
    }

}
