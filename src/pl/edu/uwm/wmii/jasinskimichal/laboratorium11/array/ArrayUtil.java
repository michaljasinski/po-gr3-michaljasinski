package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.array;

public class ArrayUtil<T extends Comparable<T>> {
    public static <T extends Comparable<T>> boolean isSorted(T[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i].compareTo(a[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    public static <T extends Comparable<T>> int binSearch(T[] tab, T a) {
        int wynik = -1;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i].equals(a)) {
                wynik = i;
            }


        }
        return wynik;
    }


    public static <T extends Comparable<T>> void selectionSort(T[] a) {
        int n = a.length;
        for (int i = 0; i < n - 1; i++) {
            int min_idx = i;
            for (int j = i + 1; j < n; j++) {
                if (a[j].compareTo(a[min_idx]) < 0)
                    min_idx = j;

            }
            if(min_idx!=i)
            {
                T temp = a[min_idx];
                a[min_idx] = a[i];
                a[i] = temp;
            }

        }

    }

    public static <T extends Comparable<T>> void mergeSort(T[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        T[] l = (T[]) new Comparable[mid];
        T[] r = (T[]) new Comparable[n-mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }

    private static <T extends Comparable<T>> void merge(T[] a, T[] l, T[] r, int left, int right) {
        {

            int i = 0, j = 0, k = 0;
            while (i < left && j < right) {
                if (l[i].compareTo(r[j]) <0 || l[i].compareTo(r[j]) == -0) {
                    a[k++] = l[i++];
                } else {
                    a[k++] = r[j++];
                }
            }
            while (i < left) {
                a[k++] = l[i++];
            }
            while (j < right) {
                a[k++] = r[j++];
            }
        }


    }
}
