package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski;



public class Osoba {


    public Osoba(String nazwisko, int rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;

    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    @Override
    public String toString() {
        return "nazwisko= " + nazwisko + ", rok urodzenia= "+rokUrodzenia;
    }

    public Osoba() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private String nazwisko;
    private int rokUrodzenia;
}
