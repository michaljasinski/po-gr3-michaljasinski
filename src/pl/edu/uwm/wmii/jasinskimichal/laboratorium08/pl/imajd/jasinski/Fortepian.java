package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

public class Fortepian extends Instrument {
    public Fortepian(String producent, int year, int month, int day) {
        super(producent, year, month, day);
    }

    @Override
    public String Dzwiek() {
        return "Fortepian: bam, bam, bam";
    }

    @Override
    public String toString() {
        return "Fortepian: "+ super.toString();
    }
}
