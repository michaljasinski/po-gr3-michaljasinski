package pl.edu.uwm.wmii.jasinskimichal.laboratorium7.pl.imiajd.jasinski;

public class Adres {


    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void pokaz()
    {
        System.out.println(kod_pocztowy+" "+miasto);
        if(numer_mieszkania!=0)
        {
            System.out.println(ulica+" "+numer_domu+"\\"+numer_mieszkania);
        }
        else
        {
            System.out.println(ulica+" "+numer_domu);
        }
    }
    public boolean przed(Adres pom)
    {
        System.out.println(Integer.parseInt(kod_pocztowy.substring(0,2)));
        if(Integer.parseInt(kod_pocztowy.substring(0,2))>Integer.parseInt(pom.kod_pocztowy.substring(0,2)))
        {
            return false;
        }
        else if(Integer.parseInt(kod_pocztowy.substring(0,2))==Integer.parseInt(pom.kod_pocztowy.substring(0,2)))
        {
            if(Integer.parseInt(kod_pocztowy.substring(4))>Integer.parseInt(pom.kod_pocztowy.substring(4)) ||Integer.parseInt(kod_pocztowy.substring(4))==Integer.parseInt(pom.kod_pocztowy.substring(4)))
            {
                return false;
            }
        }
        return true;
    }
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;
}
