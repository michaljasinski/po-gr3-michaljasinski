package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

public class Zad1a {
    public static void main(String[]args)
    {

        System.out.println(countChar("cocacola",'c'));
    }
    public static int countChar(String str,char c )
    {   int wynik=0;
        for(char i:str.toCharArray())
        {
            if(i==c)
            {
                wynik++;
            }
        }
        return wynik;
    }
}
