package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2g {
    public static void main(String [] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int[]tab=new int[n];
        generuj(tab,n,-999,999);
        for(int x:tab)
        {
            System.out.println(x);
        }
        System.out.println("Podaj lewy");
        int lewy= scanner.nextInt();
        System.out.println("Podaj prawy");
        int prawy= scanner.nextInt();
        odwrocFragment(tab,lewy,prawy);

    }
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc)
    {   Random rnd=new Random();
        int tmp;

        for(int i=0;i<n;i++)
        {
            tmp=rnd.nextInt(Math.abs(minWartosc)+maxWartosc+1)+minWartosc;
            tab[i]=tmp;


        }
    }
    public static void odwrocFragment(int []tab,int lewy,int prawy)
    {
        int pom=prawy;
        int []tab2=tab.clone();
        for(int i=lewy;i<=prawy;i++)
        {
            tab[i]=tab2[pom];
            pom--;
        }
        System.out.println("------------Po zmianie--------------");
        for(int x:tab)
        {
            System.out.println(x);
        }
    }

}
