package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski;

public class Student extends Osoba {

    public Student(String nazwisko, int rokUrodzenia,String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }


    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() + ", kierunek= "+kierunek;
    }
    private String kierunek;

    public Student(String nazwisko, int rokUrodzenia) {
        super(nazwisko, rokUrodzenia);
    }

    @Override
    public String getNazwisko() {
        return super.getNazwisko();
    }

    @Override
    public int getRokUrodzenia() {
        return super.getRokUrodzenia();
    }

    public Student() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
