package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2e {
    public static void main(String [] args)
    {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int[]tab=new int[n];
        generuj(tab,n,-999,999);
        for(int x:tab)
        {
            System.out.println(x);
        }
        System.out.println(dlugoscMaksymalnegoCiaguDodatnich(tab));
    }
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc)
    {   Random rnd=new Random();
        int tmp;

        for(int i=0;i<n;i++)
        {
            tmp=rnd.nextInt(Math.abs(minWartosc)+maxWartosc+1)+minWartosc;
            tab[i]=tmp;


        }
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {   int dlugosc=1;
        int wynik=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0 )
            {
                wynik++;
            }
            else
            {
                if(wynik>dlugosc)
                {
                    dlugosc=wynik;
                }
                wynik=0;
            }

        }
        return dlugosc;
    }
}
