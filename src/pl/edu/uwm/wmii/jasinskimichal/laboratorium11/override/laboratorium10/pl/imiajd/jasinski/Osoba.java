package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium10.pl.imiajd.jasinski;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba implements Cloneable,Comparable<Osoba> {
    public Osoba(String nazwisko, String dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = LocalDate.parse(dataUrodzenia);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(String dataUrodzenia) {
        this.dataUrodzenia = LocalDate.parse(dataUrodzenia);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    @Override
    public Osoba clone() throws CloneNotSupportedException {
        Osoba cloned = (Osoba) super.clone();
        cloned.nazwisko = nazwisko;
        cloned.dataUrodzenia = dataUrodzenia;
        return cloned;
    }

    @Override
    public int compareTo(Osoba o) {

        if(nazwisko.charAt(0)>o.nazwisko.charAt(0))
        {
            return 1;
        }
        else if(nazwisko.charAt(0)<o.nazwisko.charAt(0))
        {
            return -1;
        }
        else
        {
            if(dataUrodzenia.isBefore(o.dataUrodzenia))
            {
                return -1;
            }
            else if(!dataUrodzenia.isBefore(o.dataUrodzenia))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }



    }

    @Override
    public String toString() {
        return "Osoba["+ nazwisko + ", "  + dataUrodzenia +
                ']';
    }


    private String nazwisko;
    private LocalDate dataUrodzenia;
}
