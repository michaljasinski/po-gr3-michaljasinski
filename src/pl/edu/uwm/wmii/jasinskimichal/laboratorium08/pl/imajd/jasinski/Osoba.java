package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;

public abstract class Osoba {
    public Osoba(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.płeć = płeć;
        this.dataUrodzenia = LocalDate.of(year, month, day);
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public String getImionaOne(int i) {
        return imiona[i];
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public String getPłeć() {
        if (płeć) {
            return "Kobieta";
        }
        return "Mężczyzna";

    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean płeć;
}
