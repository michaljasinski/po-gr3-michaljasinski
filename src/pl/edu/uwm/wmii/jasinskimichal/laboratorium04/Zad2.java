package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj nazwe pliku");
        String nazwapliku=scanner.next();
        File plik = new File(nazwapliku);
        Scanner in = new Scanner(plik);
        int wynik=0;
        System.out.println("Podaj litere");
        String pom=scanner.next();
        while (in.hasNextLine() != false) {
            for(char c:in.nextLine().toCharArray())
            {
                if(c==pom.charAt(0))
                {
                    wynik++;
                }
            }
        }
        System.out.println(wynik);
    }
}
