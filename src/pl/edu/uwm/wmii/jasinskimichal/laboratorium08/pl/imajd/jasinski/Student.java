package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

public class Student extends Osoba {


    public Student(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day, double średniaOcen) {
        super(nazwisko, imiona, płeć, year, month, day);
        this.średniaOcen = średniaOcen;
    }

    public double getŚredniaOcen() {
        return średniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + kierunek + ", średnia ocen: " + średniaOcen;
    }

    private String kierunek;
    private double średniaOcen;
}
