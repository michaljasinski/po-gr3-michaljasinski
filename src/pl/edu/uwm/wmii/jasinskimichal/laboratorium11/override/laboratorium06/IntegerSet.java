package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium06;


import java.util.Arrays;

public class IntegerSet {
    boolean [] tab ;
    public IntegerSet()
    {
        tab = new boolean[100];
    }

    public static IntegerSet union(IntegerSet a, IntegerSet b)
    {   IntegerSet wynik = new IntegerSet();
        for(int i=0;i<100;i++)
        {
            if(a.tab[i]==true ||b.tab[i]==true)
            {
                wynik.tab[i]=true;
            }
        }
        return wynik;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b)
    {
        IntegerSet wynik = new IntegerSet();
        for(int i=0;i<a.tab.length;i++)
        {
            if(a.tab[i]==true && b.tab[i]==true)
            {
                wynik.tab[i]=true;
            }
        }
        return wynik;
    }
    public void insertElement(int n)
    {
        tab[n-1]=true;
    }
    public void deleteElement(int n)
    {
        tab[n-1]=false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerSet that = (IntegerSet) o;
        return Arrays.equals(tab, that.tab);
    }



    @Override
    public String toString()
    {
        StringBuilder wynik= new StringBuilder();
        for(int i=0;i<100;i++)
        {
            if(tab[i])
            {
                wynik.append(i+1+" ");
            }
        }
        return wynik.toString();
    }
}
