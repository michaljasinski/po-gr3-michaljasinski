package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

public class Zad1b {
    public static void main(String []args)
    {

        System.out.println(countSubStr("kotaalamakota","kota"));
    }
    public static int countSubStr(String str, String subStr)
    {   int j=0;
        int wynik=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==subStr.charAt(j))
            {
                j++;
            }
            else
            {
                j=0;
            }
            if(j==subStr.length()-1)
            {
                wynik++;
                j=0;
            }
        }
        return wynik;
    }
}
