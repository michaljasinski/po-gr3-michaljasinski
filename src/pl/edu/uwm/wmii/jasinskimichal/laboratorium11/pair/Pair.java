package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.pair;

public class Pair<T> {

    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }
    public   void  swap()
    {
        T pom = this.getFirst();
        this.setFirst(this.getSecond());
        this.setSecond(pom);
    }
    private T first;
    private T second;
}

