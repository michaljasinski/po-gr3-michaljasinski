package pl.edu.uwm.wmii.jasinskimichal.laboratorium12;

import java.util.Stack;

public class zad6 {
    public static void main(String[]args)
    {
        podzielnacyfry(199321);
    }
    public static void podzielnacyfry(int n)
    {   int tmp;
        Stack<Integer> stos = new Stack<>();
        while(n>0)
        {
            tmp=n % 10;
            stos.add(tmp);
            n/=10;
        }
        while(!stos.empty())
        {
            System.out.print(stos.pop()+" ");
        }
    }
}
