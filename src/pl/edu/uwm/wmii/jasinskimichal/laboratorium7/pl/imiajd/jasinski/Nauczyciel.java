package pl.edu.uwm.wmii.jasinskimichal.laboratorium7.pl.imiajd.jasinski;

public class Nauczyciel extends Osoba {




    public Nauczyciel(String nazwisko, int rokUrodzenia,double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return super.toString()+", pensja= "+pensja;
    }

    private  double pensja;
}
