package pl.edu.uwm.wmii.jasinskimichal.laboratorium10.pl.imiajd.jasinski;

public class Student extends Osoba {
    public Student(String nazwisko, String dataUrodzenia,double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    @Override
    public boolean equals(Object o) {
        if(super.equals(o)==true)
        {
            if(sredniaOcen==((Student)o).sredniaOcen)
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public Student clone() throws CloneNotSupportedException {
        ((Student)super.clone()).sredniaOcen=sredniaOcen;
        return (Student) super.clone();
    }

    @Override
    public int compareTo(Osoba o) {
        if(getNazwisko().equals(o.getNazwisko()) && getDataUrodzenia().isEqual(o.getDataUrodzenia()))
        {
        if(sredniaOcen>((Student)o).getSredniaOcen())
        {
            return 1;
        }
        else if(sredniaOcen<((Student)o).getSredniaOcen())
        {
            return -1;
        }
        else
        {
            return 0;
        }

        }

        return super.compareTo(o);
    }

    @Override
    public String toString() {
        return "Student"+super.toString().substring(5,super.toString().length()-1)+", średnia= "+sredniaOcen+"]";
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    private double sredniaOcen;
}


