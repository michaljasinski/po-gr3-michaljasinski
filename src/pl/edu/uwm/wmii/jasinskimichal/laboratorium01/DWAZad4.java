package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Arrays;
import java.util.Scanner;

public class DWAZad4 {
    public static void main(String [] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();

        int an;
        int[] tab = new int[n];


        for(int i = 0;i < n;i++)
        {   System.out.println("Podaj "+(i+1)+" liczbe");
            an = scanner.nextInt();
            tab[i]=an;
        }
        System.out.println("max = "+ Arrays.stream(tab).max().getAsInt());
        System.out.println("min = "+ Arrays.stream(tab).min().getAsInt());
    }

}
