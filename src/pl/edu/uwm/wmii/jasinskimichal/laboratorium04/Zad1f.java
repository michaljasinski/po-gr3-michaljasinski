package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

import java.util.Scanner;

public class Zad1f {
    public static void main(String[]args)
    {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String test = change(str);
        System.out.println(test);
    }
    public static String change(String str)
    {
        StringBuffer tmp =new StringBuffer();
        for(char c:str.toCharArray())
        {
            if(Character.isUpperCase(c))
            {
                tmp.append(Character.toLowerCase(c));
            }
            else if(Character.isLowerCase(c))
            {
                tmp.append(Character.toUpperCase(c));
            }
            else
            {
                tmp.append(c);
            }
        }
        String wynik = tmp.toString();
        return wynik;
    }

}
