package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7;

import pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski.Nauczyciel;
import pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski.Osoba;
import pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski.Student;

public class TestOsoba {
    public static void main(String []args)
    {
        Osoba kowal = new Osoba("Kowalski",2000);
        System.out.println(kowal);
        Student nowak = new Student("Nowak",1996,"ISI");
        System.out.println(nowak);
        Nauczyciel gora = new Nauczyciel("Góra",1990,3550.5);
        System.out.println(gora);
    }
}
