package pl.edu.uwm.wmii.jasinskimichal.laboratorium08;


import pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski.Osoba;
import pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski.Pracownik;
import pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski.Student;

public class TestOsoba
{

    public static void main(String[] args)
    {
        String[] maciek = {"Maciek","maciek"};
        String[] kuba = {"Kuba"};
        Osoba[] ludzie = new Osoba[2];


        Pracownik kowalski = new Pracownik("Kowalski", maciek, false, 2000, 5, 5, 3500, "2005-05-05");
        Student nowak = new Student("Nowak", kuba, true, 2000, 5, 5, 4.55);
        ludzie[0]=kowalski;
        ludzie[1]=nowak;
        System.out.println(kowalski.getDataZatrudnienia());
        System.out.println(kowalski.getImionaOne(1));
        System.out.println(nowak.getŚredniaOcen());

        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}


