package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski;

public class BetterRectangle extends  java.awt.Rectangle {
    /*public BetterRectangle(int x,int y,int szerokosc,int wysokosc)
    {   setLocation(x,y);
        setSize(szerokosc,wysokosc);

    }*/
    public BetterRectangle(int x,int y,int szerokosc,int wysokosc)
    {
        super(x,y,szerokosc,wysokosc);


    }

    @Override
    public String toString() {
        return "BetterRectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }


    public int getArea()
    {
        return width*height;
    }
    public int getPerimeter()
    {
        return 2*getArea();
    }
}
