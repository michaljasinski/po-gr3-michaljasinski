package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class DWAZad1a {
    public static void main(String [] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynik=0;
        int an;
        int tab[] = new int[n];

        for(int i = 0;i < n;i++)
        {
            System.out.println("Podaj "+(i+1) +" liczbe");
            an = scanner.nextInt();

            if(an%2!=0)
            {   tab[wynik]=an;
                wynik++;


            }

        }
        System.out.print("Liczby nieparzyste :");
        for(int x:tab)
        {

            if(x!=0)
                System.out.printf("%d, ",x);

        }
        System.out.println("\nIlosc liczb nieparzystych = "+wynik);
    }
}
