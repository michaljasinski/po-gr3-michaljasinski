package pl.edu.uwm.wmii.jasinskimichal.laboratorium13.Zadanie3;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zad3 {
    public static void main(String[]args)
    {
        Map<Student,String> tab = new HashMap<>();
        boolean pom = true;
        Scanner scanner = new Scanner(System.in);
        while(pom)
        {
            System.out.println("Dodaj studenta - 1");
            System.out.println("Usun studenta - 2");
            System.out.println("Zmien ocene studenta - 3");
            System.out.println("Wypisz studentow - 4");
            System.out.println("Zakoncz - 5");
            int c = scanner.nextInt();
            if(c==1)
            {
                System.out.println("Imie");
                String imie = scanner.next();
                System.out.println("Nazwisko");
                String nazwisko = scanner.next();
                System.out.println("Id");
                int id = scanner.nextInt();
                System.out.println("Ocena");
                String ocena = scanner.next();
                tab.put(new Student(imie,nazwisko,id),ocena);

            }
            else if(c==2)
            {
                System.out.println("Id studenta do usuniecia");

                for(Student s :tab.keySet())
                {
                    if (s.getId()==scanner.nextInt())
                    {
                        tab.remove(s);
                    }
                }

            }
            else if(c==3)
            {
                System.out.println("Id");
                int id = scanner.nextInt();
                for(Student s :tab.keySet())
                {
                    if (s.getId()==id)
                    {
                        System.out.println("Nowa ocena");

                        tab.replace(s,scanner.next());
                    }
                }





            }
            else if (c==4)
            {
                Map<Student, String> sort = new TreeMap<>(tab);
                for (Student stu : sort.keySet()) {
                    System.out.println(stu+" : "+sort.get(stu));

                }
                System.out.println();
            }
            else if(c==5)
            {
                pom=false;
            }
        }


    }
}
