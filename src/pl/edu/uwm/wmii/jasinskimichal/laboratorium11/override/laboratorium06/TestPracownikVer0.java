package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium06;

import java.time.LocalDate;
import java.util.Objects;


public class TestPracownikVer0 {
  
  public static void main(String[] args) {
    Pracownik[] personel = new Pracownik[3];


    personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
    personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
    personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);


    for (Pracownik e : personel) {
      e.zwiekszPobory(20);
    }


    for (Pracownik e : personel) {
      System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
      System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
    }
    System.out.println();


    


    for (Pracownik e : personel) {
      System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
      System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
    }
    System.out.println();

  }
}

class Pracownik {
  
  public Pracownik(String nazwisko, double pobory, int year, int month, int day) {
    this.nazwisko = nazwisko;
    this.pobory = pobory;
    dataZatrudnienia =  LocalDate.of(year,month,day);

  }

  public String nazwisko() {
    return nazwisko;
  }

  public double pobory() {
    return pobory;
  }

  public LocalDate dataZatrudnienia() {

    return dataZatrudnienia;


  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pracownik pracownik = (Pracownik) o;
    return Double.compare(pracownik.pobory, pobory) == 0 &&
            Objects.equals(nazwisko, pracownik.nazwisko) &&
            Objects.equals(dataZatrudnienia, pracownik.dataZatrudnienia);
  }

  @Override
  public String toString() {
    return "Pracownik{" +
            "nazwisko='" + nazwisko + '\'' +
            ", pobory=" + pobory +
            ", dataZatrudnienia=" + dataZatrudnienia +
            '}';
  }

  public void zwiekszPobory(double procent) {
    double podwyżka = pobory * procent / 100;
    pobory += podwyżka;
  }

  private String nazwisko;
  private double pobory;
  private LocalDate dataZatrudnienia;
}

