package pl.edu.uwm.wmii.jasinskimichal.laboratorium7.pl.imiajd.jasinski;

public class Student extends Osoba {

    public Student(String nazwisko, int rokUrodzenia,String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }


    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() + ", kierunek= "+kierunek;
    }
    private String kierunek;
}
