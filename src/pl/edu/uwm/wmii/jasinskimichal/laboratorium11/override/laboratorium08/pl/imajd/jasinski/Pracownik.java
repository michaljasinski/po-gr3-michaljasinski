package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;

public class Pracownik extends Osoba {


    public Pracownik(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day, double pobory, String dataZatrudnienia) {
        super(nazwisko, imiona, płeć, year, month, day);
        this.pobory = pobory;
        this.dataZatrudnienia = LocalDate.parse(dataZatrudnienia);
    }

    public double getPobory() {
        return pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł i data zatrudnienia %s", pobory, dataZatrudnienia.toString());
    }

    public Pracownik(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day) {
        super(nazwisko, imiona, płeć, year, month, day);
    }

    @Override
    public String getNazwisko() {
        return super.getNazwisko();
    }

    @Override
    public String[] getImiona() {
        return super.getImiona();
    }

    @Override
    public String getImionaOne(int i) {
        return super.getImionaOne(i);
    }

    @Override
    public LocalDate getDataUrodzenia() {
        return super.getDataUrodzenia();
    }

    @Override
    public String getPłeć() {
        return super.getPłeć();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
