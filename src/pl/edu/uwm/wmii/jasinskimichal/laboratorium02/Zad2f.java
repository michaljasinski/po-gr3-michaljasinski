package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2f {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int[]tab=new int[n];
        generuj(tab,n,-999,999);
        for(int x:tab)
        {
            System.out.println(x);
        }
        signum(tab);
        System.out.println();
        for(int x:tab)
        {
            System.out.println(x);
        }

    }
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc)
    {   Random rnd=new Random();
        int tmp;

        for(int i=0;i<n;i++)
        {
            tmp=rnd.nextInt(Math.abs(minWartosc)+maxWartosc+1)+minWartosc;
            tab[i]=tmp;


        }
    }
    public static void signum(int[]tab)
    {
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]>0)
            {
                tab[i]=1;
            }
            else if(tab[i]<0)
            {
                tab[i]=-1;
            }
        }
    }
}
