package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

public class Zad1e {
    public static void main(String[]args)
    {
        int []tab=where("Abrakadabra","dab");
        for(int x:tab)
        {
            System.out.println(x);
        }
    }
    public static int[]where(String str,String subStr)
    {
        int[] wynik=new int[subStr.length()];
        int koniec=0;
        int j=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==subStr.charAt(j))
            {
                j++;
            }
            else
            {
                j=0;
            }
            if(j==subStr.length())
            {   koniec=i;
                break;
            }
        }
        int q=0;
        for(int i=koniec-subStr.length()+1;i<=koniec;i++)
        {
            wynik[q]=i;
            q++;
        }
        return wynik;
    }
}
