package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

public class Zad1h {
    public static void main(String[]args)
    {

        System.out.println(nice("100000",2,'*'));
    }
    public static String nice(String str,int coile,char separator)
    {
        StringBuffer tmp = new StringBuffer();
        int dotrzech=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            tmp.append(str.charAt(i));
            dotrzech++;
            if(dotrzech==coile &&i!=0)
            {
                tmp.append(separator);
                dotrzech=0;
            }
        }
        String wynik = tmp.reverse().toString();
        return wynik;
    }
}
