package pl.edu.uwm.wmii.jasinskimichal.laboratorium7.pl.imiajd.jasinski;

public class NazwanyPunkt extends Punkt {
    public NazwanyPunkt(int x, int y, String name)
    {
        super(x, y);
        this.name = name;
    }

    public void show()
    {
        System.out.println(name + ":<" + x() + ", " + y() + ">");
    }

    private String name;
}

