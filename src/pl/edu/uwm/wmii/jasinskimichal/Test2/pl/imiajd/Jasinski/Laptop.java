package pl.edu.uwm.wmii.jasinskimichal.Test2.pl.imiajd.Jasinski;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {
    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }

    public Laptop(String nazwa, String dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);

        this.czyApple = czyApple;
    }

    public Laptop(String nazwa, int rok, int miesiac, int dzien, boolean czyApple) {
        super(nazwa, rok, miesiac, dzien);
        this.czyApple = czyApple;
    }

    public String isCzyApple() {

        if (czyApple) {
            return "Laptop Apple";
        }

        return "Laptop nie Apple";

    }

    public void setCzyApple(boolean czyApple) {

        this.czyApple = czyApple;
    }

    @Override
    public int compareTo(Komputer k) {

        if (super.compareTo(k) == 0) {
            Laptop laptop = (Laptop) k;
            return Boolean.compare(czyApple, laptop.czyApple);
        }

        return super.compareTo(k);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (super.equals(otherObject) == true) {
            Laptop other = (Laptop) otherObject;
            return czyApple == other.czyApple;
        }
        return super.equals(otherObject);
    }

    @Override
    public Laptop clone() throws CloneNotSupportedException {
        Laptop cloned = (Laptop) super.clone();
        return cloned;
    }

    @Override
    public String toString() {
        if (czyApple) {
            return "Laptop --Apple--: " + "nazwa: " + getNazwa() + ", data produkcji: " + getDataProdukcji();
        }
        return "Laptop: " + "nazwa: " + getNazwa() + ", data produkcji: " + getDataProdukcji();

    }

    private boolean czyApple;
}
