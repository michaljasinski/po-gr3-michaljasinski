package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj m");
        int m = scanner.nextInt();
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        System.out.println("Podaj k");
        int k = scanner.nextInt();
        int[][] macierza = macierz(m, n);
        int[][] macierzb = macierz(n, k);
        int [][] macierzc = multiply(macierza,macierzb);

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("macierzA[%d][%d] = %d\n", i, j, macierza[i][j]);
            }
        }
        System.out.println();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("macierzB[%d][%d] = %d\n", i, j, macierzb[i][j]);
            }
        }
        System.out.println();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("macierzC[%d][%d] = %d\n", i, j, macierzc[i][j]);
            }
        }

    }

    public static int[][] macierz(int x, int y) {
        int[][] tab = new int[x][];
        for (int i = 0; i < x; i++) {
            tab[i] = new int[y];
        }
        Random rnd = new Random();
        int tmp;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                tmp = rnd.nextInt(21) - 10;
                tab[i][j] = tmp;
            }
        }

        return tab;
    }

    private static int[][] multiply(int[][] first, int[][] second) {
        int row = first.length;
        int column = first[0].length;
        int[][] sum = new int[row][column];

        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                sum[i][j] = 0;
                for (int k = 0; k < row; k++) {
                    sum[i][j] += first[i][k] * second[k][j];
                }


            }



        }
        return sum;
    }
}
