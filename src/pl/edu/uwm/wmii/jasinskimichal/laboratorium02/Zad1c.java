package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zad1c {

        public static void main(String[] args)
        {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int[] tab = new int[n];
            Random rnd = new Random();
            int tmp;
            int max;
            int sumamax=0;

            for(int i=0;i<n;i++)
            {   tmp=rnd.nextInt(1999)-999;
                tab[i]=tmp;
            }
            max = Arrays.stream(tab).max().getAsInt();
            for(int x:tab)
            {
                System.out.println(x);
                if(x==max)
                {
                    sumamax++;
                }

            }
            System.out.println("najwieksza liczba to = "+max+" i pojawia się = "+sumamax+" razy");

        }
    }

