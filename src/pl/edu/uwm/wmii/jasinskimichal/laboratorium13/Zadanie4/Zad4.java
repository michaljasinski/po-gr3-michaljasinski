package pl.edu.uwm.wmii.jasinskimichal.laboratorium13.Zadanie4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class Zad4 {
    public static void main(String[]args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("test"));
        Map<Integer, HashSet<String>> tab = new HashMap<>();


        String next;
        int h;
        while (scanner.hasNext())
        {
            next=scanner.next();
            h=next.hashCode();
            if(tab.containsKey(h)==true)
            {
                tab.get(h).add(next);
            }
            else
            {
                tab.put(h,new HashSet<String >());
                tab.get(h).add(next);
            }


        }
        for(HashSet s: tab.values())
        {
            if(s.size()>1)
            {
                System.out.println(s);
            }
        }
    }
}
