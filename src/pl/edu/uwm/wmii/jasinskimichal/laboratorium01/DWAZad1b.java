package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class DWAZad1b {
    public static void main(String [] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynik=0;
        int an;

        for(int i = 0;i < n;i++)
        {
            System.out.print("Podaj "+(i+1)+ " liczbe\n");
            an = scanner.nextInt();
            if(an%3==0 && an%5!=0)
            {
                wynik++;
            }
        }
        System.out.println("Liczb które są podzielne przez 3 i niepodzielne przez 5 jest = "+wynik);
    }
}
