package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium7.pl.imiajd.jasinski;

public class Nauczyciel extends Osoba {




    public Nauczyciel(String nazwisko, int rokUrodzenia,double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return super.toString()+", pensja= "+pensja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nauczyciel that = (Nauczyciel) o;
        return Double.compare(that.pensja, pensja) == 0;
    }



    private  double pensja;
}
