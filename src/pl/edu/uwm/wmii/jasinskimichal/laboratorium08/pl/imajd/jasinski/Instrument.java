package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrument {
    public Instrument(String producent,int year,int month,int day) {
        this.producent = producent;
        this.rokProdukcji=LocalDate.of(year,month,day);
    }
    public abstract String  Dzwiek();



    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return Objects.equals(producent, that.producent) &&
                Objects.equals(rokProdukcji, that.rokProdukcji);
    }


    @Override
    public String toString() {
        return "Producent = "+producent+", Rok produkcji = "+rokProdukcji;
    }

    private String producent;
    private LocalDate rokProdukcji;
}
