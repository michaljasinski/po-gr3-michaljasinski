package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class Zad1h {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynik=0;
        int an;
        String komunikat="";
        for(int i = 0;i < n-1;i++)
        {   System.out.println("Podaj "+(i+1)+" liczbe");
            an = scanner.nextInt();
            if(i==0)
            {
                wynik+=an;
                komunikat+=(Integer.toString(an) + " + ");
                continue;
            }
            if (i % 2 != 0)
            {
                wynik+=an;
                komunikat+=(Integer.toString(an) + " - ");
            }
            else
            {
                wynik-=an;
                komunikat+=(Integer.toString(an) + " + ");
            }

        }

        komunikat=komunikat.substring(0,komunikat.length()-2);
        System.out.println("Podaj "+n+" liczbe");
        an = scanner.nextInt();
        if(n%2==0)
        {
            wynik+=Math.pow(-1,n+1)*an;
            komunikat+=( " + "+"(-1)^"+Integer.toString(n+1)+" * "+Integer.toString(an));
        }
        else
        {
            {
                wynik-=Math.pow(-1,n+1)*an;
                komunikat+=( " - "+"(-1)^"+Integer.toString(n+1)+" * "+Integer.toString(an));
            }
        }


        komunikat+=("= "+Integer.toString(wynik));
        System.out.println(komunikat);
    }
}
