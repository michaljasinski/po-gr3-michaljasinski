package pl.edu.uwm.wmii.jasinskimichal.laboratorium13.Zadanie3;

public class Student implements  Comparable<Student>{
    private String imie;
    private String nazwisko;
    private int id;

    public Student(String imie, String nazwisko, int id) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Student o) {
        if(nazwisko.compareTo(o.nazwisko)==0)
        {   if(imie.compareTo(o.imie)==0)
        {
            return Integer.compare(id,o.id);
        }
            return imie.compareTo(o.imie);
        }
        return nazwisko.compareTo(o.nazwisko);
    }

    @Override
    public String toString() {
        return nazwisko+" "+imie+" "+id;
    }
}
