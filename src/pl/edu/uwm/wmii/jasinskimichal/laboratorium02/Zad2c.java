package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zad2c {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int[]tab=new int[n];
        generuj(tab,n,-999,999);
        System.out.println(ileMaksymalnych(tab));
    }
    public static void generuj (int tab[],int n,int minWartosc,int maxWartosc)
    {   Random rnd=new Random();
        int tmp;

        for(int i=0;i<n;i++)
        {
            tmp=rnd.nextInt(Math.abs(minWartosc)+maxWartosc+1)+minWartosc;
            tab[i]=tmp;


        }
    }
    public static int ileMaksymalnych(int tab[])
    {
        int max= Arrays.stream(tab).max().getAsInt();
        int ilemax=0;
        for(int x:tab)
        {
            if(x==max)
            {
                ilemax++;
            }
        }
        return ilemax;
    }
}
