package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

public class Skrzypce extends Instrument {
    public Skrzypce(String producent, int year, int month, int day) {
        super(producent, year, month, day);
    }

    @Override
    public String Dzwiek() {
        return "Skrzypce: skrzyp, skrzyp, skrzyp";
    }

    @Override
    public String toString() {
        return "Skrzypce: "+super.toString();
    }
}
