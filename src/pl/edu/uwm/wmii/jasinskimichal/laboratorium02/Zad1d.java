package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1d {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] tab = new int[n];
        Random rnd = new Random();
        int tmp;
        int sumauj=0;
        int sumadod=0;
        for(int i=0;i<n;i++)
        {   tmp=rnd.nextInt(1999)-999;
            tab[i]=tmp;
        }
        for(int x:tab)
        {
            System.out.println(x);
            if(x>0)
            {
                sumadod+=x;
            }
            else if(x<0)
            {
                sumauj+=x;
            }
        }
        System.out.println("Suma liczb dodadnich = "+sumadod +" suma liczb ujemnyhc = "+sumauj);

    }
}
