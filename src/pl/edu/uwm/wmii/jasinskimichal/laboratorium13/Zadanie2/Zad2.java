package pl.edu.uwm.wmii.jasinskimichal.laboratorium13.Zadanie2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zad2 {
    public static void main(String[]args)
    {
        Map<String,String> tab = new HashMap<>();
        boolean pom = true;
        Scanner scanner = new Scanner(System.in);
        while(pom)
        {
            System.out.println("Dodaj studenta - 1");
            System.out.println("Usun studenta - 2");
            System.out.println("Zmien ocene studenta - 3");
            System.out.println("Wypisz studentow - 4");
            System.out.println("Zakoncz - 5");
            int c = scanner.nextInt();
            if(c==1)
            {
                System.out.println("Imie");
                String imie = scanner.next();
                System.out.println("Ocena");
                String ocena = scanner.next();
                tab.put(imie,ocena);

            }
            else if(c==2)
            {
                System.out.println("Imie studenta do usuniecia");

                tab.remove(scanner.next());

            }
            else if(c==3)
            {
                System.out.println("Imie");
                String imie = scanner.next();
                System.out.println("Nowa ocena");
                String ocena = scanner.next();
                tab.replace(imie,ocena);



            }
            else if (c==4)
            {
                Map<String, String> sort = new TreeMap<>(tab);
                for (String str : sort.keySet()) {
                    System.out.println(str+" : "+sort.get(str));

                }
                System.out.println();
            }
            else if(c==5)
            {
                pom=false;
            }
        }


    }}
