package pl.edu.uwm.wmii.jasinskimichal.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1b {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] tab = new int[n];
        Random rnd = new Random();
        int tmp;
        int uj=0;
        int dod=0;
        int zer=0;
        for(int i=0;i<n;i++)
        {   tmp=rnd.nextInt(1999)-999;
            tab[i]=tmp;
        }
        for(int x:tab)
        {
            System.out.println(x);
            if(x>0)
            {
                dod++;
            }
            else if(x<0)
            {
                uj++;
            }
            else
            {
                zer++;
            }
        }
        System.out.println("dod = "+dod+" uj = "+uj+" zer = "+zer);

    }
}
