package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;

public class Student extends Osoba {


    public Student(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day, double średniaOcen) {
        super(nazwisko, imiona, płeć, year, month, day);
        this.średniaOcen = średniaOcen;
    }

    public double getŚredniaOcen() {
        return średniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + kierunek + ", średnia ocen: " + średniaOcen;
    }

    private String kierunek;
    private double średniaOcen;

    public Student(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day) {
        super(nazwisko, imiona, płeć, year, month, day);
    }

    @Override
    public String getNazwisko() {
        return super.getNazwisko();
    }

    @Override
    public String[] getImiona() {
        return super.getImiona();
    }

    @Override
    public String getImionaOne(int i) {
        return super.getImionaOne(i);
    }

    @Override
    public LocalDate getDataUrodzenia() {
        return super.getDataUrodzenia();
    }

    @Override
    public String getPłeć() {
        return super.getPłeć();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
