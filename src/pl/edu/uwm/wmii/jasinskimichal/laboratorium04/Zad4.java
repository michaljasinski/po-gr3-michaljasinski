package pl.edu.uwm.wmii.jasinskimichal.laboratorium04;

import java.math.BigInteger;

public class Zad4 {
    public static void main(String[]args)
    {
        BigInteger wynik = foo(10);
        System.out.println(wynik);
    }

    public static BigInteger foo(int n) {
        BigInteger a = BigInteger.valueOf(0);
        BigInteger b = BigInteger.valueOf(1);
        for(int i = 0; i < n * n; i++) {
            a = a.add(b);
            b = b.multiply(BigInteger.valueOf(2));
        }

        return a;
    }
}


