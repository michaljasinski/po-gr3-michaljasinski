package pl.edu.uwm.wmii.jasinskimichal.laboratorium10;

import pl.edu.uwm.wmii.jasinskimichal.laboratorium10.pl.imiajd.jasinski.Osoba;

import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[]args) throws CloneNotSupportedException {
        Osoba kowalski = new Osoba("Jasiński","1996-02-05");
        Osoba kowalski2 = new Osoba("Kowalski","2000-01-31");
        Osoba nowak = new Osoba("Kowalski","2000-02-01");
        Osoba kowalski3 = new Osoba("Lewandowski","2000-01-01");
        Osoba ja = new Osoba("Jasiński","2000-02-05");
        ArrayList<Osoba> grupa = new ArrayList<>();
        grupa.add(kowalski);grupa.add(kowalski2);grupa.add(nowak);grupa.add(kowalski3);grupa.add(ja);
        System.out.println(kowalski.compareTo(kowalski2));
        System.out.println(nowak.compareTo(kowalski2));
        System.out.println(ja.equals(nowak));
        System.out.println(grupa);
        grupa.sort(Osoba::compareTo);

        System.out.println(grupa);
        Osoba nowakclone = nowak.clone();
        System.out.println(nowakclone);
        nowakclone.setNazwisko("Nowak");
        System.out.println(nowakclone);
        System.out.println(nowak);
    }
}
