package pl.edu.uwm.wmii.jasinskimichal.laboratorium11.override.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;

public class Fortepian extends Instrument {
    public Fortepian(String producent, int year, int month, int day) {
        super(producent, year, month, day);
    }

    @Override
    public String Dzwiek() {
        return "Fortepian: bam, bam, bam";
    }

    @Override
    public String toString() {
        return "Fortepian: "+ super.toString();
    }

    @Override
    public String getProducent() {
        return super.getProducent();
    }

    @Override
    public LocalDate getRokProdukcji() {
        return super.getRokProdukcji();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
