package pl.edu.uwm.wmii.jasinskimichal.laboratorium01;

import java.util.Scanner;

public class Zad1e {
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner (System.in);
        System.out.println("Podaj n");
        int n = scanner.nextInt();
        int wynik=1;
        int an;
        String komunikat="";
        for(int i = 0;i < n;i++)
        {   System.out.println("Podaj "+(i+1)+" liczbe");
            an = scanner.nextInt();
            wynik*= Math.abs(an);
            if(an<0)
            {
                komunikat+="|("+(Integer.toString(an) +")|" + " * ");
            }
            else {
                komunikat += ("|"+Integer.toString(an)+"|" + " * ");
            }
        }
        komunikat=komunikat.substring(0,komunikat.length()-2);
        komunikat+=("= "+Integer.toString(wynik));
        System.out.println(komunikat);
    }
}
