package pl.edu.uwm.wmii.jasinskimichal.laboratorium08.pl.imajd.jasinski;

import java.time.LocalDate;

public class Pracownik extends Osoba {


    public Pracownik(String nazwisko, String[] imiona, boolean płeć, int year, int month, int day, double pobory, String dataZatrudnienia) {
        super(nazwisko, imiona, płeć, year, month, day);
        this.pobory = pobory;
        this.dataZatrudnienia = LocalDate.parse(dataZatrudnienia);
    }

    public double getPobory() {
        return pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł i data zatrudnienia %s", pobory, dataZatrudnienia.toString());
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}
